region = "us-west-2"

vpc_cidr = "172.16.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = ""

ami-bastion = ""

ami-nginx = ""

ami-sonar = ""

ami = ""

keypair = "imayorstudios"

account_no = "840305138013"

master-password = "devopspblproject"

master-username = "mayowa"


tags = {
  Owner-Email     = "mayowaipinyomi@gmail.com"
  Managed-By      = "Terraform"
  Billing-Account = "840305138013"
}

